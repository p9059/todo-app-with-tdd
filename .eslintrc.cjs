module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
  plugins: ['svelte3', '@typescript-eslint'],
  ignorePatterns: ['*.cjs'],
  // ignorePatterns: ['static/', 'dist/', 'tools/', 'tests/', 'functions/lib/'],
  overrides: [{ files: ['*.svelte'], processor: 'svelte3/svelte3' }],
  settings: {
    'svelte3/typescript': () => require('typescript')
  },
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020
  },
  env: {
    browser: true,
    es2021: true,
    node: true,
    jest: true
  }
  // rules: {
  //   '@typescript-eslint/no-var-requires': 0,
  //   '@typescript-eslint/no-explicit-any': 0,
  //   '@typescript-eslint/no-namespace': 0,
  //   '@typescript-eslint/no-unused-vars': 'warn',
  //   '@typescript-eslint/no-empty-function': 'warn',
  //   'unused-imports/no-unused-imports': 'error',
  //   'unused-imports/no-unused-vars': 0,
  //   'prefer-const': 'warn',
  //   'no-unused-vars': 0,
  //   'no-empty': 'warn',
  //   'no-var': 'warn',
  //   'no-mixed-spaces-and-tabs': 0
  // }
};
