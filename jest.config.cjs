module.exports = {
  roots: ['<rootDir>/src'],
  transform: {
    '^.+\\.svelte$': 'svelte-jester',
    '^.+\\.c?js$': 'babel-jest'
  },
  moduleFileExtensions: ['cjs', 'js', 'svelte'],
  setupFilesAfterEnv: ['./jest-setup-after-env.cjs'],
  testEnvironment: 'jest-environment-jsdom',
  // testMatch: ['**/__tests__/**/*.c?[jt]s?(x)', '**/?(*.)+(spec|test).c?[jt]s?(x)'],
  collectCoverage: true,
  coverageDirectory: 'reports/unit',
  coverageReporters: ['html-spa', 'text-summary'],
  coverageThreshold: {
    global: {
      branches: 40,
      functions: 20,
      lines: 60,
      statements: 60
    }
  }
};
